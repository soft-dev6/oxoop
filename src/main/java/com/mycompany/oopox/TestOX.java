/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oopox;
import java.util.Scanner;
/**
 *
 * @author hanam
 */
public class TestOX {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        Game game = new Game();
        game.showWelcome();
        game.newBoard();
        while(true){
            game.showTable();
            game.showTurn();
            game.inputRowCol();
            if(game.isFinish()){
                game.showResult();
                game.showStat();
                game.newBoard();
            }
        }
        
    }
}
